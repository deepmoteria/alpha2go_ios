//
//  GetProvider.h
//  TaxiNow
//
//  Created by My Mac on 7/10/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import "RatingBar.h"
#import "ASStarRatingView.h"


@interface GetProvider : BaseVC<UITableViewDataSource,UITableViewDelegate>
{
   
}
@property (weak, nonatomic) IBOutlet UITableView *tblProviderList;
@property(strong,nonatomic)NSMutableArray *arrProviders;
@property (strong , nonatomic) NSTimer *timerForCheckReqStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;

@property (strong,nonatomic)NSString *strForLatitude;
@property (strong,nonatomic)NSString *strForLongitude;
@property (strong,nonatomic)NSString *strForTypeid;
@property (strong,nonatomic)NSString *strForAddressNote;
@property (strong,nonatomic)NSString *strPayment_Option;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelRequest;
- (IBAction)onClickCancelRequest:(id)sender;

@property (strong, nonatomic) NSDictionary *dictForCar;

@property (weak,nonatomic)UIImage *imgForCar;


@end
