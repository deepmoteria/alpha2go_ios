//
//  GetProvider.m
//  TaxiNow
//
//  Created by My Mac on 7/10/15.
//  Copyright (c) 2015 Jigs. All rights reserved.

#import "GetProvider.h"
#import "showdriverCell.h"
#import "ProviderDetailsVC.h"
#import "UIImageView+Download.h"
#import "ASStarRatingView.h"
#import "RatingBar.h"
#import "UIView+Utils.h"

@interface GetProvider()
{
    NSString *strForUserId,*strForUserToken,*strReqId,*strForDriverLatitude,*strForDriverLongitude;
}
@end

@implementation GetProvider
@synthesize arrProviders,strForLatitude,strForLongitude,strForTypeid,timerForCheckReqStatus,strPayment_Option;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super setBackBarItem];
    [[AppDelegate sharedAppDelegate]hideLoadingView];
    NSLog(@"arr = %@",arrProviders);
    
    NSLog(@"dict car info = %@",self.dictForCar);
    
    NSLog(@"image = %@",self.imgForCar);
    
    self.btnCancelRequest.hidden = YES;
    self.tblProviderList.contentInset = UIEdgeInsetsZero;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    if(![[NSUserDefaults standardUserDefaults]boolForKey:@"english"])
        self.btnMenu.frame = CGRectMake(self.btnMenu.frame.origin.x, self.btnMenu.frame.origin.y, 190, self.btnMenu.frame.size.height);
    else
        self.btnMenu.frame = CGRectMake(self.btnMenu.frame.origin.x, self.btnMenu.frame.origin.y, 140, self.btnMenu.frame.size.height);
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.btnMenu setTitle:NSLocalizedString(@"SELECT_BEAUTICAIN", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"SELECT_BEAUTICAIN", nil) forState:UIControlStateHighlighted];
    [self.btnCancelRequest setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [self.btnCancelRequest setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateHighlighted];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [timerForCheckReqStatus invalidate];
    timerForCheckReqStatus=nil;
    
    self.btnCancelRequest.hidden=YES;
}

#pragma mark
#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrProviders.count;
}
-(showdriverCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    showdriverCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProviderList"];
    
    if(cell == nil)
    {
        cell = [[showdriverCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"ProviderList"];
    }
    
    cell.lblDriverName.text = [NSString stringWithFormat:@"%@ %@",[[arrProviders objectAtIndex:indexPath.row]valueForKey:@"first_name"],[[arrProviders objectAtIndex:indexPath.row]valueForKey:@"last_name"]];
    cell.lblPrice.text = [NSString stringWithFormat:@"$%.2f",[[[arrProviders objectAtIndex:indexPath.row]valueForKey:@"total_price"] floatValue]];
    
    cell.lblCompanyName.text = [NSString stringWithFormat:@"%@",[[arrProviders objectAtIndex:indexPath.row]valueForKey:@"bio"]];

    [cell.imgDriverProfile downloadFromURL:[[arrProviders objectAtIndex:indexPath.row]valueForKey:@"picture"] withPlaceholder:nil];
    
    [cell.imgDriverProfile applyRoundedCornersFull];
    
    cell.lblRate.text = [NSString stringWithFormat:@"%@",[[arrProviders objectAtIndex:indexPath.row]valueForKey:@"rating"]];
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strForUserId=[pref objectForKey:PREF_USER_ID];
    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
    NSDictionary *dict=[arrProviders objectAtIndex:indexPath.row];
    NSString *strProviderId=[dict valueForKey:@"id"];
    
    [pref setObject:strProviderId forKey:@"PID"];
    [pref synchronize];
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:@"1" forKey:PARAM_DISTANCE];
    [dictParam setValue:strForLatitude forKey:PARAM_LATITUDE];
    [dictParam setValue:strForLongitude  forKey:PARAM_LONGITUDE];
    [dictParam setValue:strForUserId forKey:PARAM_ID];
    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
    [dictParam setValue:strForTypeid forKey:PARAM_TYPE];
    [dictParam setValue:strPayment_Option forKey:PARAM_PAYMENT_OPT];
    [dictParam setValue:strProviderId  forKey:@"provider_id"];
    //[dictParam setValue:strForIp forKey:@"ip"];
    [dictParam setValue:self.strForAddressNote forKey:@"address_note"];
    
    [dictParam setValue:[self.dictForCar valueForKey:@"make"] forKey:@"make"];
    [dictParam setValue:[self.dictForCar valueForKey:@"model"] forKey:@"model"];
    [dictParam setValue:[self.dictForCar valueForKey:@"color"] forKey:@"color"];
    [dictParam setValue:[self.dictForCar valueForKey:@"plats"] forKey:@"plates"];
    [dictParam setValue:[self.dictForCar valueForKey:@"zone"] forKey:@"zone"];
    
    //[dictParam setValue:strProviderId  forKey:@"provider_id"];
    NSLog(@" half dict = %@",dictParam);
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CREATING_REQUEST", nil)];
    
    UIImage *imgUpload = [[UtilityClass sharedObject]scaleAndRotateImage:self.imgForCar];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_CREATE_REQUEST_PROVIDERS withParamDataImage:dictParam andImage:imgUpload withBlock:^(id response, NSError *error)
     {
         // [[AppDelegate sharedAppDelegate]hideLoadingView];
         if (response)
         {
             NSLog(@"res = %@",response);
             if([[response valueForKey:@"success"]boolValue])
             {
                 NSUserDefaults *prefe = [NSUserDefaults standardUserDefaults];
                 NSString *strred=[response valueForKey:@"request_id"];
                 [prefe setObject:strred forKey:PREF_REQ_ID];
                 [prefe synchronize];
                  NSLog(@"req id :%@",strred);
                 [self.btnCancelRequest setHidden:NO];
                 [APPDELEGATE.window addSubview:self.btnCancelRequest];
                 [APPDELEGATE.window bringSubviewToFront:self.btnCancelRequest];
                 
                 timerForCheckReqStatus = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(checkForDriverRequestStatus) userInfo:nil repeats:YES];
             }
             else
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 NSString *str1 = [response valueForKey:@"error_code"];
                 if([str1 intValue] == 406)
                 {
                     [timerForCheckReqStatus invalidate];
                     timerForCheckReqStatus = nil;
                     [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                 }
                 else
                 {
                     [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                 }
             }
         }
     }];
    
}

-(void)checkForDriverRequestStatus
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        strReqId=[pref objectForKey:PREF_REQ_ID];
        NSLog(@" q :%@",strReqId);
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue] && [[response valueForKey:@"confirmed_walker"] integerValue]!=0)
                 {
                     NSLog(@"GET REQ--->%@",response);
                     NSString *strCheck=[response valueForKey:@"walker"];
                     
                     if(strCheck)
                     {
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         NSMutableDictionary *dictWalker=[response valueForKey:@"walker"];
                         strForDriverLatitude=[dictWalker valueForKey:@"latitude"];
                         strForDriverLongitude=[dictWalker valueForKey:@"longitude"];
                         if ([[response valueForKey:@"is_walker_rated"]integerValue]==1)
                         {
                             [pref removeObjectForKey:PREF_REQ_ID];
                             [pref synchronize];
                         }
                         
                         ProviderDetailsVC *vcFeed = nil;
                         for (int i=0; i<self.navigationController.viewControllers.count; i++)
                         {
                             UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
                             if ([vc isKindOfClass:[ProviderDetailsVC class]])
                             {
                                 vcFeed = (ProviderDetailsVC *)vc;
                             }
                         }
                         if (vcFeed==nil)
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus=nil;
                             [[AppDelegate sharedAppDelegate]hideLoadingView];
                             
                             self.btnCancelRequest.hidden=YES;
                             //[self.btnCancelRequest removeFromSuperview];
                             //[APPDELEGATE.window sendSubviewToBack:self.btnCancelRequest];
                             
                             [self performSegueWithIdentifier:SEGUE_TO_PROVIDER sender:self];
                         }
                         else
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus=nil;
                             [[AppDelegate sharedAppDelegate]hideLoadingView];
                             [self.navigationController popToViewController:vcFeed animated:NO];
                         }
                         
                     }
                 }
                 if([[response valueForKey:@"confirmed_walker"] intValue]==0 && [[response valueForKey:@"status"] intValue]==1)
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     [timerForCheckReqStatus invalidate];
                     timerForCheckReqStatus=nil;
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     [pref removeObjectForKey:PREF_REQ_ID];
                     
                     if([[response valueForKey:@"is_cancelled"]intValue]==1)
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_WALKER", nil)];
                     else
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_TIMEOUT", nil)];
                     
                     self.btnCancelRequest.hidden=YES;
                     // [self showMapCurrentLocatinn];
                     [APPDELEGATE hideLoadingView];
                 }
                 else
                 {
                     //[APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                     NSString *str1 = [response valueForKey:@"error_code"];
                     if([str1 intValue] == 406)
                     {
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus = nil;
                         [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                     }
                 }
             }
             else
             {
                 
             }
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:SEGUE_TO_PROVIDER])
    {
        
        ProviderDetailsVC *obj=[segue destinationViewController];
        obj.strForLatitude=strForLatitude;
        obj.strForLongitude=strForLongitude;
        obj.strForWalkStatedLatitude=strForDriverLatitude;
        obj.strForWalkStatedLongitude=strForDriverLongitude;
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



- (IBAction)onClickCancelRequest:(id)sender
{
    if([CLLocationManager locationServicesEnabled])
    {
        if([[AppDelegate sharedAppDelegate]connected])
        {
            [[AppDelegate sharedAppDelegate]hideLoadingView];
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForUserId=[pref objectForKey:PREF_USER_ID];
            strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:strReqId forKey:PARAM_REQUEST_ID];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if (response)
                 {
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus=nil;
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         [self.btnCancelRequest setHidden:YES];
                         //[self.btnCancelRequest removeFromSuperview];
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                         
                     }
                     else
                     {
                         [APPDELEGATE hideLoadingView];
                         NSString *str1 = [response valueForKey:@"error_code"];
                         if([str1 intValue] == 406)
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus = nil;
                             [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                         }
                         //[APPDELEGATE showToastMessage:[response valueForKey:@"error_messages"]];
                     }
                 }
                 
                 
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
        
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Towber -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
        
    }
    
}
@end
